#!/usr/bin/env python
# -*- coding: utf-8 -*-

def set_template(args):

    if args.template == 'SVMAP':

        args.task = 'Deblurring'

        if args.task == "Deblurring":
            args.data_train = 'BLUR_IMAGE'
            args.dir_data = '../TrainingData'
            args.data_test = 'BLUR_IMAGE'
            args.reset = False
            args.model = "deblur"
            args.test_only = True

            ### test for blurry images with 1% Gaussian noise
            args.dir_data_test = './TestData/GaussianNoise1'
            args.sigma_for_initialization = 0.01
            args.pre_train = "./model/model_SVMAP_gs1.pt"

            ### test for blurry images with 5% Gaussian noise
            '''
            args.dir_data_test = './TestData/GaussianNoise5'
            args.sigma_for_initialization = 0.05
            args.pre_train = "./model/model_SVMAP_gs5.pt"
            '''

            ### test for blurry images with saturated pixels
            '''
            args.dir_data_test = './TestData/Saturation'
            args.sigma_for_initialization = 0.01
            args.pre_train = "./model/model_SVMAP_sat.pt"
            '''

            ### test for real blurry images
            '''
            args.dir_data_test = './TestData/Real'
            args.sigma_for_initialization = 0.01
            args.pre_train = "./model/model_SVMAP_gs1.pt"
            '''

            args.save = "deblur"
            args.loss = "1*L1"
            args.patch_size = 240
            args.batch_size = 1
            args.grad_clip = 0.5
            if args.test_only:
                args.save = "deblur_test"
            args.save_results = True
            args.save_models = True
            args.no_augment = False

          

