# Learning Spatially-Variant MAP Models for Non-blind Image Deblurring

This repository is the PyTorch implementation of the paper:

**Learning Spatially-Variant MAP Models for Non-blind Image Deblurring**

Jiangxin Dong, Stefan Roth, and Bernt Schiele

To appear at CVPR 2021

[[Paper]](https://openaccess.thecvf.com/content/CVPR2021/papers/Dong_Learning_Spatially-Variant_MAP_Models_for_Non-Blind_Image_Deblurring_CVPR_2021_paper.pdf)/[[Supplemental]](https://openaccess.thecvf.com/content/CVPR2021/supplemental/Dong_Learning_Spatially-Variant_MAP_CVPR_2021_supplemental.pdf)

## Introduction

The classical maximum a-posteriori (MAP) framework for non-blind image deblurring requires defining suitable data and regularization terms, whose interplay yields the desired clear image through optimization. The vast majority of prior work focuses on advancing one of these two crucial ingredients, while keeping the other one standard. Considering the indispensable roles and interplay of both data and regularization terms, we propose a simple and effective approach to jointly learn these two terms, embedding deep neural networks within the constraints of the MAP framework, trained in an end-to-end manner. The neural networks not only yield suitable image-adaptive features for both terms, but actually predict per-pixel spatially-variant features instead of the commonly used spatially-uniform ones. The resulting spatially-variant data and regularization terms particularly improve the restoration of fine-scale structures and detail. Quantitative and qualitative results underline the effectiveness of our approach, substantially outperforming the current state of the art.

## Requirements

Compatible with Python 3

Main requirements: PyTorch 0.4.1 is tested

To install requirements:

```setup
pip install torch==0.4.1.post2 torchvision==0.2.2
pip install -r requirements.txt
```

## Evaluation

To evaluate the spatially-variant MAP model on test examples, select the corresponding options in 'template.py' and run:

```eval
python main.py
```

## Pre-trained Model

./model/model_SVMAP_gs1.pt (for blurry images with 1% Gaussian noise)

./model/model_SVMAP_gs5.pt (for blurry images with 5% Gaussian noise)

./model/model_SVMAP_sat.pt (for blurry images with saturated pixels)

## Bibtex

Please cite our paper if it is helpful to your work:

```
@InProceedings{Dong_2021_CVPR,
    author    = {Dong, Jiangxin and Roth, Stefan and Schiele, Bernt},
    title     = {Learning Spatially-Variant MAP Models for Non-Blind Image Deblurring},
    booktitle = {Proceedings of the IEEE/CVF Conference on Computer Vision and Pattern Recognition (CVPR)},
    month     = {June},
    year      = {2021},
    pages     = {4886-4895}
}
```

